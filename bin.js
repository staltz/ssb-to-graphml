#!/usr/bin/env node
const run = require('./index');

const showBlocks = process.argv.includes('--blocks');

run({showBlocks}).then(linesOfText => {
  for (let line of linesOfText) {
    process.stdout.write(line);
  }
});
