const ssbKeys = require('ssb-keys');
const ssbConfigInject = require('ssb-config/inject');
const path = require('path');
const pull = require('pull-stream');
const pify = require('pify');

function startServer() {
  const config = ssbConfigInject();
  config.keys = ssbKeys.loadOrCreateSync(path.join(config.path, 'secret'));
  config.port = 8198;
  config.logging.level = '';
  return require('ssb-server')
    .use(require('ssb-plugins'))
    .use(require('ssb-master'))
    .use(require('ssb-replicate')) // ssb-friends needs this
    .use(require('ssb-backlinks')) // ssb-friends needs this
    .use(require('ssb-friends'))
    .use(require('ssb-about'))
    .call(null, config);
}

const MIN_TS = 1.438787024567e12; // Dominic's first account

function buildGraphAttrs(sbot) {
  const attributesWithoutIds = [
    {
      id: undefined,
      about: 'node',
      name: 'label',
      type: 'string',
      build: async feedid => {
        try {
          return await pify(sbot.about.socialValue)({
            key: 'name',
            dest: feedid,
          });
        } catch (error) {
          return '';
        }
      },
    },

    {
      id: undefined,
      about: 'node',
      name: 'birth',
      type: 'double',
      build: feedid => {
        try {
          let done = false;
          return pify(cb =>
            pull(
              sbot.createUserStream({id: feedid, reverse: false, limit: 1}),
              pull.drain(
                msg => {
                  const arrivalTS = msg.timestamp;
                  const declaredTS = msg.value.timestamp;
                  const ts = Math.max(MIN_TS, Math.min(arrivalTS, declaredTS));
                  done = true;
                  cb(null, ts);
                },
                () => {
                  if (!done) {
                    cb(null, Date.now());
                  }
                },
              ),
            ),
          )();
        } catch (error) {
          return Date.now();
        }
      },
    },

    {
      id: undefined,
      about: 'node',
      name: 'lastactive',
      type: 'double',
      build: feedid => {
        try {
          let done = false;
          return pify(cb =>
            pull(
              sbot.createUserStream({id: feedid, reverse: true, limit: 1}),
              pull.drain(
                msg => {
                  const arrivalTS = msg.timestamp;
                  const declaredTS = msg.value.timestamp;
                  const ts = Math.max(MIN_TS, Math.min(arrivalTS, declaredTS));
                  done = true;
                  cb(null, ts);
                },
                () => {
                  if (!done) {
                    cb(null, Date.now());
                  }
                },
              ),
            ),
          )();
        } catch (error) {
          return Date.now();
        }
      },
    },

    {
      id: undefined,
      about: 'node',
      name: 'seq',
      type: 'int',
      build: async feedid => {
        try {
          return await pify(sbot.latestSequence)(feedid);
        } catch (error) {
          return 0;
        }
      },
    },
  ];
  const attributes = attributesWithoutIds.map((attr, i) => {
    attr.id = `d${i}`;
    return attr;
  });
  return attributes;
}

async function createNodeFor(id, attrs) {
  const node = {id};
  for (let attr of attrs) {
    node[attr.name] = await attr.build(node.id);
  }
  return node;
}

async function buildGraphNodes(sbot, edges, attrs) {
  const hopsData = await pify(sbot.friends.hops)();
  const nodes = [];
  for (let id of Object.keys(hopsData)) {
    nodes.push(await createNodeFor(id, attrs));
  }
  const isNodeMissing = id => nodes.findIndex(n => n.id === id) === -1;
  for (let [orig, dest] of edges) {
    if (isNodeMissing(orig)) nodes.push(await createNodeFor(orig, attrs));
    if (isNodeMissing(dest)) nodes.push(await createNodeFor(dest, attrs));
  }
  return nodes;
}

async function buildGraphFollowEdges(sbot) {
  const follows = await pify(sbot.friends.get)();
  const edges = Object.keys(follows)
    .map(orig =>
      Object.keys(follows[orig])
        .filter(dest => follows[orig][dest] === true)
        .map(dest => [orig, dest]),
    )
    .reduce((accumulated, current) => accumulated.concat(current), []);
  return edges;
}

async function buildGraphBlockEdges(sbot, nodes) {
  let edges = [];
  for (let node of nodes) {
    const info = await pify(sbot.friends.get)({source: node.id});
    const nodeEdges = Object.keys(info)
      .filter(dest => info[dest] === false)
      .map(dest => [node.id, dest]);
    edges = edges.concat(nodeEdges);
  }
  return edges;
}

async function buildGraph(sbot, showBlocks) {
  const attrs = buildGraphAttrs(sbot);
  const followEdges = await buildGraphFollowEdges(sbot);
  const nodes = await buildGraphNodes(sbot, followEdges, attrs);
  const edges = showBlocks
    ? await buildGraphBlockEdges(sbot, nodes)
    : followEdges;
  return {attrs, nodes, edges};
}

function* toGraphml({attrs, nodes, edges}) {
  yield '<?xml version="1.0" encoding="UTF-8"?>\n';
  yield `<graphml xmlns="http://graphml.graphdrawing.org/xmlns"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns
    http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">\n`;
  yield '  <graph id="G" edgedefault="directed">\n';
  for (let attr of attrs) {
    const i = attr.id;
    const f = attr.about;
    const n = attr.name;
    const t = attr.type;
    yield `    <key id="${i}" for="${f}" attr.name="${n}" attr.type="${t}"/>\n`;
  }
  for (let node of nodes) {
    yield `    <node id="${node.id}">\n`;
    for (attr of attrs) {
      const attrKey = attr.id;
      const attrValue = encodeURIComponent(node[attr.name]);
      yield `      <data key="${attrKey}">${attrValue}</data>\n`;
    }
    yield `    </node>\n`;
  }
  for (let [orig, dest] of edges) {
    yield `    <edge source="${orig}" target="${dest}"/>\n`;
  }
  yield '  </graph>\n</graphml>';
}

module.exports = async function run(opts) {
  const sbot = startServer();
  const graph = await buildGraph(sbot, opts.showBlocks);
  sbot.close();
  return toGraphml(graph);
};
